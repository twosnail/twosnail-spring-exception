<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<title>twosnail-spring-exception</title>
</head>
<body>
<h3>注解演示 例子</h3>
<h5><a href="./controller.do?name=success">正常页面</a></h5>
<h5><a href="./dao.do?name=system">Dao SystemException</a></h5>
<h5><a href="./dao.do?name=twosnail">Dao BusinessException</a></h5>
<h5><a href="./dao.do?name=param">Dao ParameterException</a></h5>
<h5><a href="./service.do?name=system">Service SystemException</a></h5>
<h5><a href="./service.do?name=twosnail">Service BusinessException</a></h5>
<h5><a href="./service.do?name=param">Service ParameterException</a></h5>
<h5><a href="./controller.do?name=system">Controller SystemException</a></h5>
<h5><a href="./controller.do?name=twosnail">Controller BusinessException</a></h5>
<h5><a href="./controller.do?name=param">Controller ParameterException</a></h5>
<h5><a href="./404.do?name=system">404错误</a></h5>

<h3>其他演示 例子</h3>
<h5><a href="./myself/simple/ex/resolver/jsp.do?name=twosnail">BusinessException jsp</a></h5>
<h5><a href="./myself/simple/ex/resolver/json.do?name=String">StringException json</a></h5>
<h5><a href="./myself/simple/ex/resolver/json2.do?name=String">StringException json2</a></h5>
</body>
</html>
