package com.twosnail.dao;

import org.springframework.stereotype.Repository;

import com.twosnail.exception.BusinessException;
import com.twosnail.exception.ParameterException;
import com.twosnail.exception.SystemException;

@Repository("testDao")
public class TestDao {
	public void exception(  String name ) throws Exception {
		
		if( name.equals( "twosnail" )  ) {
			throw new BusinessException("【error-info】：", "【BusinessException error】");
		}else if( name.equals( "param" )  ) {
			throw new ParameterException("【ParameterException Error】");
		}else {
			throw new SystemException("【error-info】：", "【SystemException error】");
		}
	}
}
