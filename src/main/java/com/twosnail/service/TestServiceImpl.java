package com.twosnail.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.twosnail.dao.TestDao;
import com.twosnail.exception.BusinessException;
import com.twosnail.exception.ParameterException;
import com.twosnail.exception.SystemException;
/**
 * <b>Description:</b></br> 
 * <b>Title:</b>Spring 异常处理
 * @company:Twosnail
 * @author: 两只蜗牛
 * @Date: 2015年1月7日上午9:26:34
 * @version 1.0
 */
@Service("testService")
public class TestServiceImpl implements TestService {
	@Resource
	private TestDao testDao;
	
	public void exception( String  name ) throws Exception {
		
		if( name.equals( "twosnail" )  ) {
			throw new BusinessException("【error-info】：", "【BusinessException error】");
		}else if( name.equals( "param" )  ) {
			throw new ParameterException("【ParameterException Error】");
		}else {
			throw new SystemException("【error-info】：", "【SystemException error】");
		}
	}

	public void dao( String  name ) throws Exception {
		testDao.exception( name );
	}
}
