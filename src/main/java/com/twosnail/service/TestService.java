package com.twosnail.service;

/**
 * <b>Description:</b></br> 
 * <b>Title:</b>Spring 异常处理
 * @company:Twosnail
 * @author: 两只蜗牛
 * @Date: 2015年1月7日上午9:26:34
 * @version 1.0
 */
public interface TestService {
	
	public void exception( String name ) throws Exception;
	
	public void dao( String name ) throws Exception;
}
