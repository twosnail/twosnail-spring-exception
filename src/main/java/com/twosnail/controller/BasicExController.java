package com.twosnail.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ExceptionHandler;

import com.twosnail.exception.BusinessException;
import com.twosnail.exception.SystemException;
/**
 * <b>Title:</b>Spring 异常处理
 * @company:Twosnail
 * @author: 两只蜗牛
 * @Date: 2015年1月7日上午9:26:34
 * @version 1.0
 */
public class BasicExController {
	
	/** 基于@ExceptionHandler异常处理 */
	//(value = { BusinessException.class, ParameterException.class, Exception.class})
	@ExceptionHandler
	public String exp(HttpServletRequest request, Exception ex) {
		
		// 根据不同错误转向不同页面  
        if( ex instanceof BusinessException ) {
            return "business-error";  
        }else if( ex instanceof SystemException ) { 
            return "system-error";
        } else {
            return "error";  
        }
	}
}