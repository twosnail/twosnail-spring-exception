package com.twosnail.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.twosnail.exception.BusinessException;
import com.twosnail.exception.ParameterException;
import com.twosnail.exception.SystemException;
import com.twosnail.service.TestService;
/**
 * <b>Description:</b>注解异常测试类</br> 
 * <b>Title:</b>Spring 异常处理
 * @company:Twosnail
 * @author: 两只蜗牛
 * @Date: 2015年1月7日上午9:26:34
 * @version 1.0
 */
@Controller
public class DemoController extends BasicExController {
	@Resource
	private TestService testService;
	
	@RequestMapping(value = "/controller.do", method = RequestMethod.GET)
	public ModelAndView controller( HttpServletResponse response, String name ) throws Exception {
		
		if( name.equals( "twosnail" )  ) {
			throw new BusinessException("【error-info】：", "【BusinessException error】");
		}else if( name.equals( "param" )  ) {
			throw new ParameterException("【ParameterException Error】");
		}else if( name.equals( "system" ) ) {
			throw new SystemException("【error-info】：", "【SystemException error】");
		}
		return new ModelAndView( "/success" ) ;
	}
	
	@RequestMapping(value = "/service.do", method = RequestMethod.GET)
	public void service(HttpServletResponse response , String name ) throws Exception {
		testService.exception( name );
	}
	
	@RequestMapping(value = "/dao.do", method = RequestMethod.GET)
	public void dao(HttpServletResponse response, String name ) throws Exception {
		testService.dao( name );
	}
	
	/**
	 * try catch之后就不会进入异BasicExController
	 * @author: Administrator
	 * @Date: 2015年1月7日
	 * @param response
	 * @param name
	 */
	@RequestMapping(value = "/dao2.do", method = RequestMethod.GET)
	public void dao2(HttpServletResponse response, String name )  {
		
		try {
			testService.dao( name );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}