package com.twosnail.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.twosnail.exception.BusinessException;
import com.twosnail.exception.ParameterException;
import com.twosnail.exception.StringException;
import com.twosnail.exception.SystemException;
/**
 * <b>Description:</b>注解异常测试类</br> 
 * <b>Title:</b>Spring 异常处理
 * @company:Twosnail
 * @author: 两只蜗牛
 * @Date: 2015年1月7日上午9:26:34
 * @version 1.0
 */
@Controller
public class MyselfSimpleExResolverController {
	
	/**
	 * jsp
	 * @author: Administrator
	 * @Date: 2015年1月7日
	 * @param response
	 * @param name
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/myself/simple/ex/resolver/jsp.do", method = RequestMethod.GET)
	public ModelAndView controllerJsp( HttpServletResponse response, String name ) throws Exception {
		
		if( name.equals( "twosnail" )  ) {
			throw new BusinessException("【error-info】：", "【BusinessException error】");
		}else if( name.equals( "param" )  ) {
			throw new ParameterException("【ParameterException Error】");
		}else if( name.equals( "system" ) ) {
			throw new SystemException("【error-info】：", "【SystemException error】");
		}
		return new ModelAndView( "/success" ) ;
	}
	
	/**
	 * json
	 * @author: Administrator
	 * @Date: 2015年1月7日
	 * @param response
	 * @param name
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/myself/simple/ex/resolver/json.do", method = RequestMethod.GET)
	@ResponseBody
	public String controllerJson( HttpServletResponse response, String name ) throws Exception {
		
		if( name.equals( "String" )  ) {
			throw new StringException("【error-info】：", "【BusinessException error】");
		}else if( name.equals( "param" )  ) {
			throw new ParameterException("【ParameterException Error】");
		}else {
			throw new SystemException("【error-info】：", "【SystemException error】");
		}
	}
	
	
	@RequestMapping(value = "/myself/simple/ex/resolver/json2.do", method = RequestMethod.GET)
	@ResponseBody
	public String controllerJson2( HttpServletResponse response, String name ) throws Exception {
		
		if( name.equals( "String" )  ) {
			throw new StringException("【error-info】：", "【BusinessException error】");
		}
		return "string" ;
	}
	
}